import Vue from 'vue'
import VueX from 'vuex'

Vue.use(VueX)

export default new VueX.Store({
  state: {
    count: 0,
    items: []
  },
  mutations: {
    changeCount (state, count) {
      state.count = count
    },
    addItem (state, item) {
      state.items.push(item)
    }
  },
  actions: {
    addTodo ({ commit, state }, item) {
      commit('addItem', item)
      commit('changeCount', state.items.length)
    }
  },
  getters: {
    countText (state) {
      return 'Items ' + state.count
    }
  }
})
