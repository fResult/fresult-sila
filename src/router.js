import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home'
import About from './views/About'
import Product from './views/Product'
import JavaTutorial from './views/JavaTutorial'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/home',
      name: 'home',
      component: Home
    },
    {
      path: '/about',
      name: 'about',
      component: About
    },
    {
      path: '/product/:id',
      name: 'product',
      component: Product
    },
    {
      path: '/java-tutorial',
      name: 'java-tutorial',
      component: JavaTutorial
    }
  ]
})
